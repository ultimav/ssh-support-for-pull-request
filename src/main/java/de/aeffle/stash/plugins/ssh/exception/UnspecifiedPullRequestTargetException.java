package de.aeffle.stash.plugins.ssh.exception;

public class UnspecifiedPullRequestTargetException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static String errorMessage = "The target repository ('to') could not be found";
    
    public UnspecifiedPullRequestTargetException() {
        super(errorMessage );
    }
    
}
