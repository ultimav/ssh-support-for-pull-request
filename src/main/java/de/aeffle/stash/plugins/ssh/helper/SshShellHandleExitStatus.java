package de.aeffle.stash.plugins.ssh.helper;

public enum SshShellHandleExitStatus {
    SUCCESS(0), ERROR(1);
    private int id;
    
    SshShellHandleExitStatus(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
};

