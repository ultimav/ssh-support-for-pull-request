package de.aeffle.stash.plugins.ssh;

import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.scm.ssh.ExitCodeCallback;
import com.atlassian.stash.scm.ssh.SshScmRequest;
import com.atlassian.stash.scm.ssh.SshScmRequestHandler;

import de.aeffle.stash.plugins.ssh.helper.SshRequestCommand;
import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.StashServiceHandler;
import de.aeffle.stash.plugins.ssh.requestHandler.CommentPullRequestHandler;
import de.aeffle.stash.plugins.ssh.requestHandler.CreatePullRequestHandler;
import de.aeffle.stash.plugins.ssh.requestHandler.DeclinePullRequestHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.io.InputStream;
import java.io.OutputStream;

public class SshRequestHandler implements SshScmRequestHandler {
	private final PullRequestService pullRequestService;
	private final RepositoryService repositoryService;
	private final NavBuilder navBuilder;
	
	public SshRequestHandler(PullRequestService pullRequestService, RepositoryService repositoryService, NavBuilder navBuilder) {
		this.pullRequestService = pullRequestService;
		this.repositoryService = repositoryService;
		this.navBuilder = navBuilder;
	}
	
	@Nullable
	@Override
	public SshScmRequest create(@Nonnull String remoteCommand,
			@Nonnull InputStream inputStream, 
			@Nonnull OutputStream outputStream,
			@Nonnull OutputStream errorStream,
			@Nonnull ExitCodeCallback exitCodeCallback) {
	    
	    SshShellHandle sshShellHandle = new SshShellHandle(inputStream, outputStream, errorStream, exitCodeCallback);
	    StashServiceHandler stashServiceHandler = new StashServiceHandler();
	    stashServiceHandler.add(pullRequestService);
	    stashServiceHandler.add(repositoryService);
	    stashServiceHandler.add(navBuilder);
	    
	    return getSshScmRequestByCommand(remoteCommand, sshShellHandle, stashServiceHandler);
	}
	

    private SshScmRequest getSshScmRequestByCommand(String remoteCommand,
            SshShellHandle sshShellHandle,
            StashServiceHandler stashServiceHandler) {
        if (SshRequestCommand.PULL_REQUEST.isMatchFor(remoteCommand))
	        return new CreatePullRequestHandler(remoteCommand, sshShellHandle, stashServiceHandler);
	        
		if (SshRequestCommand.DECLINE_PULL_REQUEST.isMatchFor(remoteCommand)) 
            return new DeclinePullRequestHandler(remoteCommand, sshShellHandle, stashServiceHandler);

		if (SshRequestCommand.COMMENT_PULL_REQUEST.isMatchFor(remoteCommand))
		    return new CommentPullRequestHandler(remoteCommand, sshShellHandle, stashServiceHandler);

		return commandNotSupported();
    }
	
	private SshScmRequest commandNotSupported() {
	    return null;
	}
}
