package ut.de.aeffle.stash.plugins.ssh.helper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;

import de.aeffle.stash.plugins.ssh.helper.CommentCommand;
import de.aeffle.stash.plugins.ssh.helper.SshRequestCommand;
import de.aeffle.stash.plugins.ssh.helper.StashServiceHandler;

public class CommentCommandTest {
    private static final String PROJECT_KEY = "projectKey";
    private static final String REPOSITORY_SLUG = "repositorySlug";
    private static final int REPOSITORY_ID = 123;
    private static final long PULL_REQUEST_ID = 123L;
    
    private CommentCommand commentCommand;

    @Before
    public void setUp() throws Exception {
        StashServiceHandler stashServiceHandler = new StashServiceHandler();
        PullRequestService pullRequestService = mock(PullRequestService.class);
        stashServiceHandler.add(pullRequestService);
        
        RepositoryService repositoryService = mock(RepositoryService.class);
        Repository repository = mock(Repository.class);
        when(repositoryService.getBySlug(PROJECT_KEY, REPOSITORY_SLUG)).thenReturn(repository);
        when(repository.getId()).thenReturn(REPOSITORY_ID);
        
        stashServiceHandler.add(repositoryService);
        
        String remoteCommand = SshRequestCommand.COMMENT_PULL_REQUEST + " " +
                PROJECT_KEY + " " +
                REPOSITORY_SLUG + " " + 
                PULL_REQUEST_ID;
        
        this.commentCommand = new CommentCommand(stashServiceHandler, remoteCommand);
    }

    @Test
    public void testGetRepositoryId() {
        assertEquals(REPOSITORY_ID, commentCommand.getRepositoryId());
    }

    @Test
    public void testGetPullRequestId() {
        assertEquals(PULL_REQUEST_ID, commentCommand.getPullRequestId());
    }

}
