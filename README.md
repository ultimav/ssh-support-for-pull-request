## Create a pull request:

To create a pull request via the ssh api you call ssh with the command *pull-request*:

```
ssh git@stash.server.url -p7999 pull-request < STDIN
```


This command uses the same json object as defined in the [REST API](https://developer.atlassian.com/static/rest/stash/2.7.1/stash-rest.html)
(/rest/api/1.0/projects/{projectKey}/repos/{repositorySlug}/pull-requests -> POST)


The following describes a complete example:
```
$ cat a.json

{
    "title": "My Pull Request",
    "description": "This is a description of the pull-request.",
    "state": "OPEN",
    "fromRef": {
        "id": "refs/heads/other",
        "repository": {
            "slug": "rep_1",
            "name": null,
            "project": {
                "key": "PROJECT_1"
            }
        }
    },
    "toRef": {
        "id": "refs/heads/master",
        "repository": {
            "slug": "rep_1",
            "name": null,
            "project": {
                "key": "PROJECT_1"
            }
        }
    },
    "reviewers": [
        {
            "user": {
                "name": "admin"
            }
        }
    ]
}
$ cat a.json | ssh git@stash.server.url -p7999 pull-request
Received the following pull request:

Title: asdf
From: (TOOL/asdf) refs/heads/other
To: (TOOL/asdf) refs/heads/master

Successfully created the pull-request
```

## Comment a pull request:

For commenting on a pull request the command is *comment pull-request*:

```
ssh git@stash.server.url -p7999 comment pull-request $PROJECT_KEY $REPOSITORY_SLUG $PULL_REQUEST_ID < STDIN
```

This command also expects the same json object as defined in the [REST API](https://developer.atlassian.com/static/rest/stash/2.7.1/stash-rest.html)
(/rest/api/1.0/projects/{projectKey}/repos/{repositorySlug}/pull-requests/{pullRequestId}/comments -> POST)


The following describes a complete example:
```
$ cat comment.json
{
  text : "asdfjklö"
}
$ cat comment.json | ssh git@stash.server.url -p7999 comment pull-request TOOL asdf 1
Successfully commented the pull-request.
```

## Decline a pull request:

For declining a pull request the command is *decline pull-request*:

```
ssh git@stash.server.url -p7999 decline pull-request $PROJECT_KEY $REPOSITORY_SLUG $PULL_REQUEST_ID [optional: $PULL_REQUEST_VERSION]
```


The following describes a complete example:
```
$ ssh git@stash.server.url -p7999 decline pull-request TOOL asdf 1 0
Successfully declined the pull-request.
```
